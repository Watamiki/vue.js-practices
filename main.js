// indent!
const app = Vue.createApp({
  data() {
    return {
      todo: '',
      todos: []
    }
  },
  methods: {
    addTodo() {
      this.todos.push(this.todo)
      console.log(this.todos)
    }
  }
})

app.mount('#app')